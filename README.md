# Chromium Tweaks

This package intended for Debian Stable aims to provide a better (but less private) experience with Chromium.

- [x] Turn on syncronization feature by default with custom settings.
- [x] Syncronization between devices using oauth2 credentials.
- [x] Add Widevine support for DRM content.

## Chromium Config Adjustments

This package intended for Debian Stable aims to provide:

* Syncronization feature by default with custom settings.
* Syncronization between devices using oauth2 credentials.
* Custom HTML page on startup.

## Chromium Widevine Downloader

This package intended for Debian Stable aims to provide the Widevine DRM plugin for Chromium.

The binary is not provided here, but downloads it from the Google website.

* **Note**: Internet connection is required.

### Credits

* [Ruario](https://gist.github.com/ruario) for the [GitHub Gist](https://gist.github.com/ruario/3c873d43eb20553d5014bd4d29fe37f1).
